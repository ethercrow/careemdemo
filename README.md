

Building instructions
---------------------

The app uses Alamofire libraries for networking, please use Cocoapods to fetch deps:

```
$ pod install
$ open CareemDemo.xcworkspace

Build CareemDemo for device or simulator
```

Discussion
----------

The task had some conflicting requirements, so I'm documenting some of my assumptions here:

 * The biggest conflict was "no more than 3 hours" and "production-ready". I
   haven't been tracking time spent on the assignment, but it turned out to be
   somewhere in 10-15 hour range, but I still consider the code far from
   "production-ready". Some of the hours were me getting up to speed with some
   recent additions to UIKit (like UIStackView) and getting familiar with
   Alamofire, but still, working on the assignment itself took significantly more
   than 3 hours
 * UI is done in code, not in Interface Builder. I've been on projects that did
   either and have some preference toward code because of git-friendliness.
 * I'm unrealistically assuming that the backend always works, is always available
   and network behaves well
 * Regarding pagination scheme: I assume that when the user is looking at search
   results, new movies are not being added at the backend, because that would shift
   page breaks and I'm not trying to deal with that in any way. The API would be
   more usable in that regard if it provided a way to ask for a pageful of items
   after an item with a given id instead of just page by page
 * I think I did separation of concerns in a reasonable manner (separate View files,
   separate WebAPI file). Persistence code is not extracted, because there's so
   little of it *at the moment*
 * No localization

Other points:

 * I didn't try to make a pleasant UI, it's optimized for debugging instead with
   different elements having different colors
 * I've chosen to have immutable model structs to prioritize correctness and
   ease of debugging and refactoring over performance
 * There are currently no tests, but the most of the code is either UI or net
   code
 * The application will crash if a necessary field in JSON response is missing.
   This is obviously unacceptable in a "production-ready" app
 * I've tested the app manually only on an iPhone SE with iOS 11
 * Posters are always fetched for 2x screen scale
 * I haven't done any performance work like CPU/GPU profiling, leak detection or
   other more iOS-specific investigation. I absolutely enjoy working with Instruments
   app, in particular, I was very impressed with Thread Sanitizer when I was
   working on a Swift library earlier this year. But again, I had to decide
   to not touch performance area due to time constraints

TODOs are all fixable of course, but free time is limited, especially during the
Holiday season. I hope that what is there is enough to have enough of an
impression of how I write code.