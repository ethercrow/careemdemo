
import Foundation
import Alamofire

func makeSearchUrl(_ maybeQuery: String?, page: Int) -> URL? {
    if let q = maybeQuery {
        let apiKey = "2696829a81b1b5827d515ff121700838"
        let urlString = String(
            format: "https://api.themoviedb.org/3/search/movie?api_key=%@&query=%@&page=%d",
            apiKey, q.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, page)
        return URL(string: urlString)
    }
    return nil
}

func loadSomeMovies(_ ml: MovieList, completion: @escaping (MovieList) -> ()) {
    guard let p: Int = ml.nextPage else {return}

    let query = ml.query
    if let url = makeSearchUrl(query, page: p) {
        Alamofire.request(url).responseJSON(queue: DispatchQueue.global(qos: .background)) { response in
            debugPrint("Response: %@", String(describing: response.response))
            debugPrint("Result: %@", response.result)
            // FIXME: handle unexpected data format
            let jsonResult = response.result.value as! [String : Any]
            let jsonMovies = jsonResult["results"] as! [[String : Any]]
            let movies: [Movie] = jsonMovies.map {d in Movie(dict: d)}
            let nextPage: Int = p + 1
            let totalPages: Int = jsonResult["total_pages"] as! Int

            // XXX: for some reason type inference cannot manage this expression
            //      when written as a ternary operator, so we resort to a var.
            var mbNextPage: Int? = nextPage
            if totalPages < nextPage {
                mbNextPage = .none
            }

            completion(
                MovieList(
                    movies: ml.movies + movies,
                    query: ml.query,
                    nextPage: mbNextPage))
        }
    } else {
        debugPrint("Couldn't form a search url with query", query)
    }
}
