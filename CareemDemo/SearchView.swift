import UIKit

class SearchView: UIView {
    public let submitButton = UIButton(type: UIButtonType.system)
    public let textField = UITextField()
    public let queryView = UITableView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.orange

        self.textField.placeholder = "The Arrival of a Train"
        self.textField.addTarget(self, action: #selector(onTextChanged(_:)), for: UIControlEvents.allEditingEvents)
        defer { self.onTextChanged(self.textField) }

        self.submitButton.backgroundColor = UIColor.green
        self.submitButton.setTitle("Submit", for: UIControlState.normal)

        self.queryView.backgroundColor = UIColor.cyan
        self.initLayout()
    }

    func initLayout() {
        self.queryView.frame = self.bounds
        self.queryView.frame.origin.y = 50
        self.queryView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.addSubview(self.queryView)

        let hstack = UIStackView(arrangedSubviews: [self.textField, self.submitButton])
        hstack.frame = self.bounds
        hstack.frame.size.height = 50
        hstack.autoresizingMask = [.flexibleWidth]
        hstack.alignment = .center
        self.addSubview(hstack)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("called SearchView.init(coder:)")
    }

    @objc func onTextChanged(_ textField: UITextField) {
        if textField.text == "" {
            self.submitButton.isUserInteractionEnabled = false
            self.submitButton.backgroundColor = .red
        } else {
            self.submitButton.isUserInteractionEnabled = true
            self.submitButton.backgroundColor = .green
        }
    }
}
