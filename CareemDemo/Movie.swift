
import Foundation
import UIKit

struct Movie {
    let title: String
    let releaseDate: String
    let posterURL: URL?
    let overview: String

    init(dict: [String: Any]) {
        // FIXME: handle malformed dicts
        self.title = dict["title"] as! String
        self.releaseDate = dict["release_date"] as! String

        self.posterURL = (dict["poster_path"] as? String).flatMap {posterPath in
            URL(string: String(format: "https://image.tmdb.org/t/p/w185%@", posterPath))
        }
        debugPrint("posterURL: %@", String(describing: self.posterURL))
        self.overview = dict["overview"] as! String
    }
}

struct MovieList {
    let movies: [Movie]
    let query: String
    let nextPage: Int?
}
