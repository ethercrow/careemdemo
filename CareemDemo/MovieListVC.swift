import UIKit

class MovieListVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    let tableView = UITableView()
    var movieList: MovieList
    var isLoadingMore = false

    init(_ movieList: MovieList) {
        self.movieList = movieList
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("called MovieListVC.init(coder:)")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .green
        self.view.addSubview(self.tableView)
        self.tableView.frame = self.view.bounds
        self.tableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }

    func tableView(_: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (section == 0) ? self.movieList.movies.count : 1
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let movie = self.movieList.movies[indexPath.row]
            let cell: MovieCell? = tableView.dequeueReusableCell(withIdentifier: "movie") as? MovieCell
            switch cell {
            case let .some(cell):
                cell.movie = movie
                return cell
            default:
                return MovieCell(movie)
            }
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "more")
                ?? UITableViewCell(style: .default, reuseIdentifier: "more")
            cell.textLabel?.text = "Wait, there's more!"
            return cell
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        switch self.movieList.nextPage {
        case .some(_):
            // first section is movies, second section is "There's more" cell
            return 2
        case .none:
            return 1
        }
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }

    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        guard let _ = self.movieList.nextPage else {return}
        let targetY = targetContentOffset.pointee.y
        let nearBottomY = 150 * (self.movieList.movies.count - 1) - Int(self.tableView.bounds.size.height)
        if (Int(targetY) >= nearBottomY) {
            if (!self.isLoadingMore) {
                self.isLoadingMore = true
                DispatchQueue.global(qos: .background).async {
                    loadSomeMovies(self.movieList, completion: { newMovieList in
                        DispatchQueue.main.async {
                            self.movieList = newMovieList
                            self.tableView.reloadData()
                            self.isLoadingMore = false
                        }
                    })
                }
            }
            debugPrint("will scroll to the end")
        }
    }
}
