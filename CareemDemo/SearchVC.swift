import UIKit

class SearchVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    let searchView = SearchView()
    var queries: [String] = UserDefaults.standard.stringArray(forKey: "queries") ?? []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Search"
        self.searchView.frame = self.view.bounds
        self.searchView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.searchView.submitButton.addTarget(self, action: #selector(onSearchTap(button:)), for: .touchUpInside)
        self.searchView.queryView.delegate = self
        self.searchView.queryView.dataSource = self
        self.view.addSubview(self.searchView)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.searchView.queryView.reloadData()
    }

    @objc func onSearchTap(button: UIButton) {
        self.doSearch(self.searchView.textField.text!)
    }

    func doSearch(_ query: String) {
        let emptyMovieList = MovieList(movies: [], query: query, nextPage: 1)
        loadSomeMovies(emptyMovieList) { newMovieList in
            DispatchQueue.main.async {
                if newMovieList.movies.count == 0 {
                    let alert = UIAlertController(title: "No results for this query", message: "", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Okay :(", style: .default))
                    self.present(alert, animated: true, completion: nil)
                } else {
                    self.queries = self.queries.filter({$0 != query})
                    self.queries.insert(query, at: 0)
                    self.queries = Array(self.queries.prefix(10))
                    UserDefaults.standard.set(self.queries, forKey: "queries")
                    self.searchView.queryView.reloadData()
                    let vc = MovieListVC(newMovieList)
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.doSearch(self.queries[indexPath.row])
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.queries.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "query")
        cell.textLabel!.text = self.queries[indexPath.row]
        return cell
    }
}
