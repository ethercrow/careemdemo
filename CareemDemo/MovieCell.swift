import UIKit

import Alamofire
import AlamofireImage

class MovieCell: UITableViewCell {
    static let reuseIdentifier = "movie"

    var movie: Movie {
        didSet {
            self.textLabel?.text = self.movie.title
            self.detailTextLabel?.text = "Release date: " + self.movie.releaseDate + "\n" + self.movie.overview
            let placeholder = UIImage(named: "poster_placeholder")!
            if let posterURL = self.movie.posterURL {
                self.imageView?.af_setImage(
                    withURL: posterURL,
                    placeholderImage: placeholder,
                    filter: AspectScaledToFillSizeFilter(size: placeholder.size))
            } else {
                self.imageView?.image = placeholder
            }
            NSLog("didSet movie %@, image size: %@", movie.title, String(describing: self.imageView?.image?.size))
        }
    }

    init(_ movie: Movie) {
        self.movie = movie
        super.init(style: .subtitle, reuseIdentifier: MovieCell.reuseIdentifier)
        self.contentView.backgroundColor = .yellow
        self.detailTextLabel?.numberOfLines = 8
        self.isUserInteractionEnabled = false

        // deferred setting triggers didSet
        defer { self.movie = movie }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("called MovieCell.init(coder:)")
    }
}
